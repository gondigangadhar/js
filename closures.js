// Closure is concept of function + lexical environment in which function it was created . so every function declared within
// the another function then it has access to the scope chain of outer function and the variables created within the scope of outer
// function will not get destroyed.

// Whenever, a function is created the variables that are available for that function, that scope captured by that function and remains
// available

// It means that eventhough createCounter is done with its execution, i think the scope is not destoyed, because count function
// still needs it.

// one more important thing to observe is for c1 and c2 there will seperate copy of variables

// if the function retuns anything except a function the memory would have garbage collected

// similar to window object in browser, we have globalThis in nodejs, but we dont have window in nodejs

function createCounter(init, delta) {
  let x = init;
  let y = delta;
  function count() {
    x = x + y;
    return x;
  }
  return count;
}

const c1 = createCounter(10, 5);
const c2 = createCounter(12, 2);

console.log(c1(), c1(), c1());
console.log(c2(), c2(), c2());
console.log(c1(), c1(), c1());

function a() {
  let x = 10;
  let p = 1;
  function b() {
    let x = 20;
    let q = 1;
    function c() {
      let x = 30;
      let r = 3;
      function d() {
        let x = 40;
        let s = p + q + r;
      }
      return d;
    }
    return c;
  }
  return b;
}
a()()()();
