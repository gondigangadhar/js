// A “higher-order function” is a function that accepts functions as parameters and/or returns a function.

// JavaScript Functions: First-Class Objects
// JavaScript functions are first-class objects. Therefore:

// They have built-in properties and methods, such as the name property and the .toString() method.
// Properties and methods can be added to them.
// They can be passed as arguments and returned from other functions.
// They can be assigned to variables, array elements, and other objects.

// Lets take an example, assume we have an array out of it we should filter the array wth certain conditions

//  case 1 : filter only odd
//  case 2 : filter only even
//  case 3 : filter only positive ones and list goes on

//  To solve above problem, we can design an HOF, which takes array as an argument and accepts a callback as well, which takes care of filtering process

function filterFunction(arr, callback) {
  const filteredArr = [];
  for (let i = 0; i < arr.length; i++) {
    callback(arr[i]) ? filteredArr.push(arr[i]) : null;
  }
  return filteredArr;
}

console.log(filterFunction([1, 2, 3, 4, 5, 6], (x) => x % 2 === 0));
console.log(filterFunction([1, 2, 3, 4, 5, 6], (x) => x % 2 === 1));
