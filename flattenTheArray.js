// const flattenArray = (arr) => {
//   let flattenedArray = [];
//   arr.forEach((item) => {
//     if (Array.isArray(item)) flattenedArray.push(...flattenArray(item));
//     else flattenedArray.push(item);
//   });
//   return flattenedArray;
// };

function flattenArray(arr, res = []) {
  arr.forEach((ele) => {
    if (typeof ele === "number") {
      res.push(ele);
    } else {
      return flattenArray(ele, res);
    }
  });
  return res;
}

console.log("result", flattenArray([[1], 2, [3, 4, [5, 6, 7]], 8]));
